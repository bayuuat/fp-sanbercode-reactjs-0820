import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { UserContext } from '../context/UserContext';
import { Layout, Menu, Image } from 'antd';
import Logo from '../img/logo.png';
import '../App.css';

const { Header } = Layout;

const logos = {
    margin: '0 24px 0 0',
    float: 'left',
};

const Nav = () => {
    const [user, setUser] = useContext(UserContext);

    const handleLogout = () => {
        setUser(null);
        localStorage.removeItem('user');
    };

    return (
        <header>
            <Layout className="layout">
                <Header>
                    <Image width={155} src={Logo} style={logos} />
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={['1']}
                        style={{ float: 'right' }}
                    >
                        <Menu.Item key="1">
                            <Link to="/">Home</Link>
                        </Menu.Item>
                        {user === null && (
                            <Menu.Item key="4">
                                <Link to="/login">Login</Link>
                            </Menu.Item>
                        )}
                        {user === null && (
                            <Menu.Item key="5">
                                <Link to="/register">Register</Link>
                            </Menu.Item>
                        )}
                        {user && (
                            <Menu.Item key="6">
                                <Link to="/editdata"> Edit Data</Link>
                            </Menu.Item>
                        )}
                        {user && (
                            <Menu.Item key="7" onClick={handleLogout}>
                                Log Out
                            </Menu.Item>
                        )}
                    </Menu>
                </Header>
            </Layout>
        </header>
    );
};

export default Nav;
