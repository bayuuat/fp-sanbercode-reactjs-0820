import React, { useContext, useState } from 'react';
import { UserContext } from '../../context/UserContext';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import { Form, Input, Button } from 'antd';

const Register = () => {
    let history = useHistory();
    const [, setUser] = useContext(UserContext);
    const [input, setInput] = useState({ name: '', email: '', password: '' });

    const handleSubmit = () => {
        axios
            .post('https://backendexample.sanbersy.com/api/register', {
                name: input.name,
                email: input.email,
                password: input.password,
            })
            .then((res) => {
                console.log(res);
                var user = res.data.user;
                var token = res.data.token;
                var currentUser = { name: user.name, email: user.email, token };
                setUser(currentUser);
                localStorage.setItem('user', JSON.stringify(currentUser));
                history.push('/login');
            })
            .catch((err) => {
                alert(err);
            });
    };

    const handleChange = (event) => {
        let value = event.target.value;
        let name = event.target.name;
        switch (name) {
            case 'name': {
                setInput({ ...input, name: value });
                break;
            }
            case 'email': {
                setInput({ ...input, email: value });
                break;
            }
            case 'password': {
                setInput({ ...input, password: value });
                break;
            }
            default: {
                break;
            }
        }
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    console.log(input.name);
    return (
        <div className="container">
            <Form
                name="basic"
                onFinish={handleSubmit}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="Name"
                    name="name"
                    type="text"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Name!',
                        },
                    ]}
                >
                    <Input
                        name="name"
                        onChange={handleChange}
                        value={input.name}
                    />
                </Form.Item>

                <Form.Item
                    label="Email"
                    name="email"
                    type="email"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Email!',
                        },
                    ]}
                >
                    <Input
                        name="email"
                        onChange={handleChange}
                        value={input.email}
                    />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    type="password"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                >
                    <Input.Password
                        name="password"
                        onChange={handleChange}
                        value={input.password}
                    />
                </Form.Item>

                <>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </>
            </Form>
        </div>
    );
};

export default Register;
