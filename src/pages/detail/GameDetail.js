import React, { useContext, useState, useEffect } from 'react';
import axios from 'axios';
import { Link, useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom';

const GameDetail = () => {
    let { id } = useParams();
    const history = useHistory();
    const [input, setInput] = useState({
        name: '',
        genre: '',
        singlePlayer: '',
        multiplayer: '',
        platform: '',
        release: 2020,
        image_url: '',
        id: null,
    });
    const [dataGameDetail, setDataGameDetail] = useState(null);

    useEffect(() => {
        if (dataGameDetail === null && id) {
            axios
                .get(`https://backendexample.sanbersy.com/api/data-game/${id}`)
                .then((res) => {
                    setDataGameDetail(res.data);
                    setInput({
                        name: res.data.name,
                        genre: res.data.genre,
                        singlePlayer: res.data.singlePlayer,
                        multiplayer: res.data.multiplayer,
                        platform: res.data.platform,
                        release: res.data.release,
                        image_url: res.data.image_url,
                        id: res.data.id,
                    });
                });
        }
    }, [dataGameDetail, setInput, id]);

    return (
        <div className="container">
            {dataGameDetail !== null &&
                [dataGameDetail].map((value, index) => (
                    <div style={{ display: 'flex' }}>
                        <img src={value.image_url} width={400} />
                        <div style={{ marginLeft: 60 }}>
                            <h1 style={{ marginBottom: 30 }}>{value.name}</h1>
                            <div>
                                <h4>Deskripsi</h4>
                                <p>Single Player : {value.singlePlayer}</p>
                                <p>Multiplayer : {value.multiplayer}</p>
                                <p>Platform : {value.platform}</p>
                                <p>Release : {value.release}</p>
                            </div>
                        </div>
                    </div>
                ))}
        </div>
    );
};

export default GameDetail;
