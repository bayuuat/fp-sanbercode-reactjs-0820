import React, { Component } from 'react';
import axios from 'axios';
import { Card, Carousel } from 'antd';
import { Link, useHistory } from 'react-router-dom';

const { Meta } = Card;

const contentStyle = {
    height: '500px',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    background: '#364d79',
};

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            movies: [],
            game: [],
        };
    }

    componentDidMount() {
        axios
            .get(`https://backendexample.sanbersy.com/api/data-movie`)
            .then((res) => {
                let movies = res.data.map((el) => {
                    return {
                        id: el.id,
                        title: el.title,
                        rating: el.rating,
                        duration: el.duration,
                        genre: el.genre,
                        description: el.description,
                        image_url: el.image_url,
                    };
                });
                this.setState({ movies });
            });
        axios
            .get(`https://backendexample.sanbersy.com/api/data-game`)
            .then((res) => {
                let game = res.data.map((el) => {
                    return {
                        id: el.id,
                        name: el.name,
                        rating: el.rating,
                        duration: el.duration,
                        genre: el.genre,
                        description: el.description,
                        image_url: el.image_url,
                    };
                });
                this.setState({ game });
            });
    }

    render() {
        return (
            <>
                <Carousel autoplay effect="fade">
                    <div>
                        <h3 style={contentStyle}>1</h3>
                    </div>
                    <div>
                        <h3 style={contentStyle}>2</h3>
                    </div>
                    <div>
                        <h3 style={contentStyle}>3</h3>
                    </div>
                    <div>
                        <h3 style={contentStyle}>4</h3>
                    </div>
                </Carousel>
                <div className="container">
                    <div>
                        <h1>Daftar Film</h1>
                        <div id="article-list">
                            {this.state.movies.map((item) => {
                                return (
                                    <div className="card-holder" key={item.id}>
                                        <Link to={`/movie/${item.id}`}>
                                            <Card
                                                hoverable
                                                style={{ width: 190 }}
                                                cover={
                                                    <img
                                                        style={{
                                                            height: 250,
                                                            objectFit: 'cover',
                                                        }}
                                                        alt="CoverMovie"
                                                        src={item.image_url}
                                                    />
                                                }
                                            >
                                                <Meta title={item.title} />
                                            </Card>
                                        </Link>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                    <div>
                        <h1>Daftar Game</h1>
                        <div id="article-list">
                            {this.state.game.map((item) => {
                                return (
                                    <div className="card-holder" key={item.id}>
                                        <Link to={`/game/${item.id}`}>
                                            <Card
                                                hoverable
                                                style={{ width: 190 }}
                                                cover={
                                                    <img
                                                        style={{
                                                            height: 250,
                                                            objectFit: 'cover',
                                                        }}
                                                        alt="CoverMovie"
                                                        src={item.image_url}
                                                    />
                                                }
                                            >
                                                <Meta title={item.name} />
                                            </Card>
                                        </Link>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Home;
