import React, { useContext, useState, useEffect } from 'react';
import axios from 'axios';
import { Table, Button, Tag, Space } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import { Layout, Menu, Breadcrumb } from 'antd';
import {
    UserOutlined,
    LaptopOutlined,
    NotificationOutlined,
} from '@ant-design/icons';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

const GameList = () => {
    const history = useHistory();
    const [dataGame, setDataGame] = useState(null);
    const [search, setSearch] = useState('');
    const [filteredGames, setFilteredGames] = useState(null);

    useEffect(() => {
        if (dataGame === null) {
            axios
                .get(`https://backendexample.sanbersy.com/api/data-game`)
                .then((res) => {
                    setDataGame(
                        res.data.map((el) => {
                            return {
                                id: el.id,
                                name: el.name,
                                genre: el.genre,
                                singlePlayer: el.singlePlayer,
                                multiplayer: el.multiplayer,
                                platform: el.platform,
                                release: el.release,
                                image_url: el.image_url,
                            };
                        })
                    );
                });
        }
    }, [dataGame]);

    return (
        <Layout>
            <Layout>
                <Sider
                    width={220}
                    style={{ height: '100vh' }}
                    className="site-layout-background"
                >
                    <Menu
                        theme="dark"
                        defaultSelectedKeys={['1']}
                        mode="inline"
                        style={{ height: '100%', borderRight: 0 }}
                    >
                        <Menu.Item key="1" icon={<UserOutlined />}>
                            <Link to="/editdata">Dashboard</Link>
                        </Menu.Item>
                        <Menu.Item key="2" icon={<LaptopOutlined />}>
                            <Link to="/editdata/editmovie">Movie</Link>
                        </Menu.Item>
                        <Menu.Item key="3" icon={<NotificationOutlined />}>
                            <Link to="/editdata/editgame">Game</Link>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout style={{ padding: '0 24px 24px' }}>
                    <Content
                        className="site-layout-background"
                        style={{
                            padding: 24,
                            margin: 0,
                            minHeight: 280,
                        }}
                    >
                        DASHBOARD COMING SOON
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    );
};

export default GameList;
