import React from 'react';
import Main from './layout/Main';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less
import { UserProvider } from './context/UserContext';
// import './App.css';

function App() {
    return (
        <UserProvider>
            <Main />
        </UserProvider>
    );
}

export default App;
