import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from './Header';
import Section from './Section';
import Footer from './Footer';

const Main = () => {
    return (
        <>
            <Router>
                <Header />
                <Section style={{ padding: '0 50px' }} />
                <Footer />
            </Router>
        </>
    );
};

export default Main;
