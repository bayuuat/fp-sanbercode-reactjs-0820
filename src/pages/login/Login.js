import React, { useContext, useState } from 'react';
import { UserContext } from '../../context/UserContext';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import { Form, Input, Button } from 'antd';

const Login = () => {
    let history = useHistory();
    const [, setUser] = useContext(UserContext);
    const [input, setInput] = useState({ email: '', password: '' });

    const handleSubmit = () => {
        axios
            .post('https://backendexample.sanbersy.com/api/user-login', {
                email: input.email,
                password: input.password,
            })
            .then((res) => {
                var user = res.data.user;
                var token = res.data.token;
                var currentUser = { name: user.name, email: user.email, token };
                setUser(currentUser);
                localStorage.setItem('user', JSON.stringify(currentUser));
                history.push('/editdata');
            })
            .catch((err) => {
                alert(err);
            });
    };

    const handleChange = (event) => {
        let value = event.target.value;
        console.log(value);
        let name = event.target.name;
        switch (name) {
            case 'email': {
                setInput({ ...input, email: value });
                break;
            }
            case 'password': {
                setInput({ ...input, password: value });
                break;
            }
            default: {
                break;
            }
        }
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div className="container">
            <Form
                name="basic"
                initialValues={{ remember: true }}
                onFinish={handleSubmit}
                onFinishFailed={onFinishFailed}
            >
                <Form.Item
                    label="Email"
                    name="email"
                    value={input.email}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Email!',
                        },
                    ]}
                >
                    <Input
                        name="email"
                        onChange={handleChange}
                        value={input.email}
                    />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    value={input.password}
                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                >
                    <Input.Password
                        name="password"
                        onChange={handleChange}
                        value={input.password}
                    />
                </Form.Item>

                <>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </>
            </Form>
        </div>
    );
};

export default Login;
