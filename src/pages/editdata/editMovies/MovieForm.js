import React, { useContext, useState, useEffect } from 'react';
import { UserContext } from '../../../context/UserContext';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

const MovieForm = () => {
    let { id } = useParams();
    let history = useHistory();
    const [dataMovieDetail, setDataMovieDetail] = useState(null);
    const [inputData, setInputData] = useState({
        title: '',
        description: '',
        year: 2020,
        duration: 120,
        genre: '',
        rating: 0,
        image_url: '',
        id: null,
    });
    const [user] = useContext(UserContext);

    useEffect(() => {
        if (dataMovieDetail === null) {
            axios
                .get(`https://backendexample.sanbersy.com/api/data-movie/${id}`)
                .then((res) => {
                    setDataMovieDetail(res.data);
                    setInputData({
                        title: res.data.title,
                        description: res.data.description,
                        year: res.data.year,
                        duration: res.data.duration,
                        genre: res.data.genre,
                        rating: res.data.rating,
                        image_url: res.data.image_url,
                        id: res.data.id,
                    });
                });
        }
    }, [dataMovieDetail, setDataMovieDetail, setInputData]);

    const handleChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        switch (name) {
            case 'title': {
                setInputData({ ...inputData, title: value });
                break;
            }
            case 'description': {
                setInputData({ ...inputData, description: value });
                break;
            }
            case 'year': {
                setInputData({ ...inputData, year: value });
                break;
            }
            case 'duration': {
                setInputData({ ...inputData, duration: value });
                break;
            }
            case 'genre': {
                setInputData({ ...inputData, genre: value });
                break;
            }
            case 'rating': {
                setInputData({ ...inputData, rating: value });
                break;
            }
            case 'image_url': {
                setInputData({ ...inputData, image_url: value });
                break;
            }
            default: {
                break;
            }
        }
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        if (inputData.id === null) {
            axios
                .post(
                    `https://www.backendexample.sanbersy.com/api/data-movie`,
                    {
                        title: inputData.title,
                        description: inputData.description,
                        year: inputData.year,
                        duration: inputData.duration,
                        genre: inputData.genre,
                        rating: parseInt(inputData.rating),
                        image_url: inputData.image_url,
                    },
                    { headers: { Authorization: `Bearer ${user.token}` } }
                )
                .then((res) => {
                    console.log(res);
                    let singleMovie = [dataMovieDetail].find(
                        (el) => el.id === inputData.id
                    );
                    singleMovie.title = inputData.title;
                    singleMovie.description = inputData.description;
                    singleMovie.year = inputData.year;
                    singleMovie.duration = inputData.duration;
                    singleMovie.genre = inputData.genre;
                    singleMovie.rating = inputData.rating;
                    setDataMovieDetail([...dataMovieDetail]);

                    setInputData({
                        title: '',
                        description: '',
                        year: 2020,
                        duration: 120,
                        genre: '',
                        rating: 0,
                        image_url: '',
                    });
                    history.push(`/editdata/editmovie`);
                })
                .catch((err) => {
                    console.log(err);
                });
        } else {
            axios
                .put(
                    `https://backendexample.sanbersy.com/api/data-movie/${id}`,
                    {
                        title: inputData.title,
                        description: inputData.description,
                        year: inputData.year,
                        duration: inputData.duration,
                        genre: inputData.genre,
                        rating: parseInt(inputData.rating),
                        image_url: inputData.image_url,
                    },
                    { headers: { Authorization: `Bearer ${user.token}` } }
                )
                .then((res) => {
                    let newDataGames = [dataMovieDetail].map((x) => {
                        if (x.id === { id }) {
                            x.title = inputData.title;
                            x.description = inputData.description;
                            x.year = inputData.year;
                            x.duration = inputData.duration;
                            x.genre = inputData.genre;
                            x.rating = inputData.rating;
                            x.image_url = inputData.image_url;
                        }
                        return x;
                    });
                    setDataMovieDetail(dataMovieDetail);
                    setInputData({
                        title: '',
                        description: '',
                        year: 2020,
                        duration: 120,
                        genre: '',
                        rating: 0,
                        image_url: '',
                    });
                    history.push(`/editdata/editmovie`);
                });
        }
    };

    return (
        <div className="container">
            {user !== null && inputData !== null && (
                <>
                    {console.log({ dataMovieDetail })}
                    {console.log({ id })}
                    <h1 style={{ textAlign: 'center' }}>Movies Form</h1>
                    <form
                        onSubmit={handleSubmit}
                        style={{
                            textAlign: 'center',
                            width: '60%',
                            margin: '0 auto',
                        }}
                    >
                        <div>
                            <label style={{ float: 'left' }}>Title:</label>
                            <input
                                style={{ float: 'right' }}
                                type="text"
                                name="title"
                                value={inputData.title}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <div>
                            <label style={{ float: 'left' }}>
                                Description:
                            </label>
                            <textarea
                                style={{ float: 'right' }}
                                type="text"
                                name="description"
                                value={inputData.description}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <div style={{ marginTop: '20px' }}>
                            <label style={{ float: 'left' }}>Year:</label>
                            <input
                                style={{ float: 'right' }}
                                type="number"
                                max={2020}
                                min={1980}
                                name="year"
                                value={inputData.year}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <div style={{ marginTop: '20px' }}>
                            <label style={{ float: 'left' }}>Duration:</label>
                            <input
                                style={{ float: 'right' }}
                                type="number"
                                name="duration"
                                value={inputData.duration}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <div style={{ marginTop: '20px' }}>
                            <label style={{ float: 'left' }}>Genre:</label>
                            <input
                                style={{ float: 'right' }}
                                type="text"
                                name="genre"
                                value={inputData.genre}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <div style={{ marginTop: '20px' }}>
                            <label style={{ float: 'left' }}>Rating:</label>
                            <input
                                style={{ float: 'right' }}
                                type="number"
                                max={10}
                                min={0}
                                name="rating"
                                value={inputData.rating}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <div style={{ marginTop: '20px' }}>
                            <label style={{ float: 'left' }}>Image Url:</label>
                            <textarea
                                style={{ float: 'right' }}
                                cols="50"
                                rows="3"
                                type="text"
                                name="image_url"
                                value={inputData.image_url}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <br />
                        <br />
                        <button>submit</button>
                    </form>
                </>
            )}
        </div>
    );
};

export default MovieForm;
