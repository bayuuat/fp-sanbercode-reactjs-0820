import React, { useContext, useState, useEffect } from 'react';
import { UserContext } from '../../../context/UserContext';
import axios from 'axios';
import { Table, Button, Input, Space } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import { Layout, Menu, Checkbox, Collapse } from 'antd';
import {
    SearchOutlined,
    UserOutlined,
    LaptopOutlined,
    NotificationOutlined,
} from '@ant-design/icons';
import Highlighter from 'react-highlight-words';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;
const { Panel } = Collapse;

const GameList = () => {
    const history = useHistory();
    const [dataGame, setDataGame] = useState(null);
    const [user] = useContext(UserContext);
    const [search, setSearch] = useState('');
    const [filter, setFilter] = useState([]);
    const [filteredGenre, setFilteredGenre] = useState({});
    const [sortedInfo, setSortedInfo] = useState(null);

    useEffect(() => {
        if (dataGame === null) {
            axios
                .get(`https://backendexample.sanbersy.com/api/data-game`)
                .then((res) => {
                    setDataGame(
                        res.data.map((el) => {
                            return {
                                id: el.id,
                                name: el.name,
                                genre: el.genre,
                                singlePlayer: el.singlePlayer,
                                multiplayer: el.multiplayer,
                                platform: el.platform,
                                release: el.release,
                                image_url: el.image_url,
                            };
                        })
                    );
                });
        }
    }, [dataGame]);

    const handleDelete = (id) => {
        axios
            .delete(`https://backendexample.sanbersy.com/api/data-game/${id}`, {
                headers: { Authorization: `Bearer ${user.token}` },
            })
            .then((res) => {
                var newDataPeserta = dataGame.filter((x) => x.id !== id);
                setDataGame(newDataPeserta);
            });
    };

    const handleEdit = (id) => {
        history.push(`./editgame/${id}`);
    };

    const handleCreate = (event) => {
        history.push(`./creategame`);
    };

    function truncateString(str, num) {
        if (str === null) {
            return '';
        } else {
            if (str.length <= num) {
                return str;
            }
            return str.slice(0, num) + '...';
        }
    }

    const columns = [
        {
            title: 'Cover',
            key: 'image_url',
            render: (item, data) => (
                <img
                    style={{
                        height: 150,
                        width: 125,
                        objectFit: 'cover',
                    }}
                    src={data.image_url}
                />
            ),
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: (item, data) => <p>{truncateString(data.name, 20)}</p>,
            sorter: {
                compare: (a, b) => a.name.length - b.name.length,
            },
        },
        {
            title: 'Genre',
            dataIndex: 'genre',
            key: 'genre',
            render: (item, data) => <p>{truncateString(data.genre, 20)}</p>,
            sorter: {
                compare: (a, b) => a.genre.length - b.genre.length,
            },
        },
        {
            title: 'Single Player',
            dataIndex: 'singlePlayer',
            key: 'singlePlayer',
            sorter: {
                compare: (a, b) => a.singlePlayer - b.singlePlayer,
            },
        },
        {
            title: 'Multiplayer',
            dataIndex: 'multiplayer',
            key: 'multiplayer',
            sorter: {
                compare: (a, b) => a.multiplayer - b.multiplayer,
            },
        },
        {
            title: 'Platform',
            dataIndex: 'platform',
            key: 'platform',
            render: (item, data) => <p>{truncateString(data.platform, 20)}</p>,
            sorter: {
                compare: (a, b) => a.platform.length - b.platform.length,
            },
        },
        {
            title: 'Release',
            dataIndex: 'release',
            key: 'release',
            sorter: {
                compare: (a, b) => a.release - b.release,
            },
        },
        {
            title: 'Action',
            dataIndex: 'action',
            key: 'id',
            render: (text, data) => (
                <Space size="middle">
                    <Button onClick={() => handleEdit(data.id)}>Edit</Button>
                    <Button onClick={() => handleDelete(data.id)}>
                        Delete
                    </Button>
                </Space>
            ),
        },
    ];

    function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
    }

    const submitSearch = (e) => {
        e.preventDefault();
        axios
            .get(`https://www.backendexample.sanbersy.com/api/data-game`)
            .then((res) => {
                let resMovies = res.data.map((el) => {
                    return {
                        id: el.id,
                        name: el.name,
                        genre: el.genre,
                        singlePlayer: el.singlePlayer,
                        multiplayer: el.multiplayer,
                        platform: el.platform,
                        release: el.release,
                        image_url: el.image_url,
                    };
                });

                let filteredMovies = resMovies.filter(
                    (x) =>
                        x.name.toLowerCase().indexOf(search.toLowerCase()) !==
                        -1
                );
                setDataGame([...filteredMovies]);
            });
    };

    const handleFilter = () => {};

    const handleChangeSearch = (e) => {
        setSearch(e.target.value);
    };

    const handleChangeFilter = (value) => {
        const currentIndex = filter.indexOf(value);
        const newChecked = [...filter];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        setFilter(newChecked);
        console.log(filter);
    };

    return (
        <Layout>
            <Layout>
                <Sider
                    width={220}
                    style={{ minHeight: '100vh' }}
                    className="site-layout-background"
                >
                    <Menu
                        theme="dark"
                        defaultSelectedKeys={['1']}
                        mode="inline"
                        style={{ height: '100%', borderRight: 0 }}
                    >
                        <Menu.Item key="editdata" icon={<UserOutlined />}>
                            <Link to="/editdata">Dashboard</Link>
                        </Menu.Item>
                        <Menu.Item key="editmovie" icon={<LaptopOutlined />}>
                            <Link to="/editdata/editmovie">Movie</Link>
                        </Menu.Item>
                        <Menu.Item
                            key="editgame"
                            icon={<NotificationOutlined />}
                        >
                            <Link to="/editdata/editgame">Game</Link>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout style={{ padding: '0 24px 24px' }}>
                    <Content
                        className="site-layout-background"
                        style={{
                            padding: 24,
                            margin: 0,
                            minHeight: 280,
                        }}
                    >
                        <div
                            style={{
                                display: 'flex',
                                width: '100%',
                                marginBottom: 20,
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                        >
                            <form
                                onSubmit={submitSearch}
                                style={{
                                    display: 'flex',
                                    width: '100%',
                                    marginBottom: 20,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <Input
                                    type="text"
                                    value={search}
                                    onChange={handleChangeSearch}
                                    style={{ width: '50%', marginRight: 20 }}
                                    size="large"
                                />
                                <Button size="large">Search</Button>
                            </form>
                        </div>
                        <Button
                            style={{ marginBottom: 20, float: 'right' }}
                            size="large"
                            onClick={() => handleCreate()}
                        >
                            + New Game
                        </Button>
                        <Collapse>
                            <Panel header key="1">
                                {dataGame !== null &&
                                    dataGame.map((value, index) => (
                                        <React.Fragment key={index}>
                                            <Checkbox
                                                onChange={() =>
                                                    handleChangeFilter(value.id)
                                                }
                                                type="checkbox"
                                                checked={
                                                    filter.indexOf(value.id) ===
                                                    -1
                                                        ? false
                                                        : true
                                                }
                                            />
                                            <span>{value.genre}</span>
                                        </React.Fragment>
                                    ))}
                            </Panel>
                        </Collapse>
                        <Table
                            columns={columns}
                            dataSource={dataGame}
                            onChange={onChange}
                        />
                    </Content>
                </Layout>
            </Layout>
        </Layout>
        // <div className="container">
        //     <h1 style={{ textAlign: 'center' }}>Daftar Peserta Lomba</h1>
        //     <Table
        //         columns={columns}
        //         dataSource={dataGame}
        //         onChange={onChange}
        //     />
        //     <h1 style={{ textAlign: 'center' }}>Daftar Peserta Lomba</h1>
        //     <button onClick={handleCreate}>+</button>
        //     <table>
        //         <thead>
        //             <tr>
        //                 <th>No</th>
        //                 <th>Name</th>
        //                 <th>Genre</th>
        //                 <th>singlePlayer</th>
        //                 <th>multiplayer</th>
        //                 <th>platform</th>
        //                 <th>release</th>
        //                 <th>Action</th>
        //             </tr>
        //         </thead>
        //         <tbody>
        //             {dataGame !== null &&
        //                 dataGame.map((item, index) => {
        //                     return (
        //                         <tr key={index}>
        //                             <td>{index + 1}</td>
        //                             <td>{item.name}</td>
        //                             <td title={item.genre}>{item.genre}</td>
        //                             <td>{item.singlePlayer}</td>
        //                             <td>{item.multiplayer}</td>
        //                             <td>{item.platform}</td>
        //                             <td>{item.release}</td>
        //                             <td>
        //                                 <button
        //                                     onClick={handleEdit}
        //                                     value={item.id}
        //                                 >
        //                                     Edit
        //                                 </button>
        //                                 &nbsp;
        //                                 <button
        //                                     onClick={handleDelete}
        //                                     value={item.id}
        //                                 >
        //                                     Delete
        //                                 </button>
        //                             </td>
        //                         </tr>
        //                     );
        //                 })}
        //         </tbody>
        //     </table>
        // </div>
    );
};

export default GameList;
