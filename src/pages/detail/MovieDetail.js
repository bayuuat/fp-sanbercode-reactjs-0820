import React, { useContext, useState, useEffect } from 'react';
import axios from 'axios';
import { Link, useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom';

const GameDetail = () => {
    let { id } = useParams();
    let history = useHistory();
    const [dataMovieDetail, setDataMovieDetail] = useState(null);

    useEffect(() => {
        if (dataMovieDetail === null) {
            axios
                .get(`https://backendexample.sanbersy.com/api/data-movie/${id}`)
                .then((res) => {
                    setDataMovieDetail(res.data);
                });
        }
    }, [dataMovieDetail, setDataMovieDetail]);

    return (
        <div className="container">
            {dataMovieDetail !== null &&
                [dataMovieDetail].map((value, index) => (
                    <div style={{ display: 'flex' }}>
                        <img src={value.image_url} width={400} />
                        <div style={{ marginLeft: 60 }}>
                            <h1 style={{ marginBottom: 30 }}>{value.title}</h1>
                            <h3 style={{ marginBottom: 30 }}>{value.genre}</h3>
                            <div>
                                <h4>Deskripsi</h4>
                                <p>{value.description}</p>
                                <p>Year : {value.year}</p>
                                <p>Duration : {value.duration}</p>
                                <p>Rating : {value.rating}</p>
                            </div>
                        </div>
                    </div>
                ))}
        </div>
    );
};

export default GameDetail;
