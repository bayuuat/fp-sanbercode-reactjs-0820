import React, { useContext, useState, useEffect } from 'react';
import { UserContext } from '../../../context/UserContext';
import axios from 'axios';
import { useHistory, Link } from 'react-router-dom';
import { Layout, Menu, Button, Space, Table, Image } from 'antd';
import {
    UserOutlined,
    LaptopOutlined,
    NotificationOutlined,
} from '@ant-design/icons';
import './Movies.css';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

const MovieList = () => {
    let history = useHistory();
    const [dataMovie, setDataMovie] = useState(null);
    const [user] = useContext(UserContext);
    const [search, setSearch] = useState('');
    const [filteredMovies, setFilteredMovies] = useState(null);

    useEffect(() => {
        if (dataMovie === null) {
            axios
                .get(`https://backendexample.sanbersy.com/api/data-movie`)
                .then((res) => {
                    setDataMovie(
                        res.data.map((el) => {
                            return {
                                id: el.id,
                                title: el.title,
                                description: el.description,
                                year: el.year,
                                duration: el.duration,
                                genre: el.genre,
                                rating: el.rating,
                                image_url: el.image_url,
                            };
                        })
                    );
                });
        }
    }, [dataMovie]);

    const handleDelete = (id) => {
        axios
            .delete(
                `https://backendexample.sanbersy.com/api/data-movie/${id}`,
                { headers: { Authorization: `Bearer ${user.token}` } }
            )
            .then((res) => {
                var newDataPeserta = dataMovie.filter((x) => x.id !== id);
                setDataMovie(newDataPeserta);
            });
    };

    const handleEdit = (id) => {
        history.push(`./editmovie/${id}`);
    };

    const handleCreate = (event) => {
        history.push(`./createmovie`);
    };

    function truncateString(str, num) {
        if (str === null) {
            return '';
        } else {
            if (str.length <= num) {
                return str;
            }
            return str.slice(0, num) + '...';
        }
    }

    function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
    }

    const columns = [
        {
            title: 'Cover',
            key: 'image_url',
            render: (item, data) => (
                <img
                    style={{
                        height: 150,
                        width: 125,
                        objectFit: 'cover',
                    }}
                    src={data.image_url}
                />
            ),
        },
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            render: (item, data) => <p>{truncateString(data.title, 20)}</p>,
            sorter: {
                compare: (a, b) => a.title.length - b.title.length,
            },
        },
        {
            title: 'Genre',
            dataIndex: 'genre',
            key: 'genre',
            render: (item, data) => <p>{truncateString(data.genre, 20)}</p>,
            sorter: {
                compare: (a, b) => a.genre.length - b.genre.length,
            },
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
            render: (item, data) => (
                <p>{truncateString(data.description, 100)}</p>
            ),
            sorter: {
                compare: (a, b) => a.description.length - b.description.length,
            },
        },
        {
            title: 'Year',
            dataIndex: 'year',
            key: 'year',
            sorter: {
                compare: (a, b) => a.year - b.year,
            },
        },
        {
            title: 'Duration',
            dataIndex: 'duration',
            key: 'duration',
            sorter: {
                compare: (a, b) => a.duration - b.duration,
            },
        },
        {
            title: 'Rating',
            dataIndex: 'rating',
            key: 'rating',
            sorter: {
                compare: (a, b) => a.rating - b.rating,
            },
        },
        {
            title: 'Action',
            key: 'action',
            render: (item, data) => (
                <Space size="middle">
                    <Button onClick={() => handleEdit(data.id)}>Edit</Button>
                    <Button onClick={() => handleDelete(data.id)}>
                        Delete
                    </Button>
                </Space>
            ),
        },
    ];

    return (
        <Layout>
            <Layout>
                <Sider
                    width={220}
                    style={{ minHeight: '100vh' }}
                    className="site-layout-background"
                >
                    <Menu
                        theme="dark"
                        defaultSelectedKeys={['1']}
                        mode="inline"
                        style={{ height: '100%', borderRight: 0 }}
                    >
                        <Menu.Item key="1" icon={<UserOutlined />}>
                            <Link to="/editdata">Dashboard</Link>
                        </Menu.Item>
                        <Menu.Item key="2" icon={<LaptopOutlined />}>
                            <Link to="/editdata/editmovie">Movie</Link>
                        </Menu.Item>
                        <Menu.Item key="3" icon={<NotificationOutlined />}>
                            <Link to="/editdata/editgame">Game</Link>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout style={{ padding: '0 24px 24px' }}>
                    <Content
                        className="site-layout-background"
                        style={{
                            padding: 24,
                            margin: 0,
                            minHeight: 280,
                        }}
                    >
                        <Button
                            style={{ marginBottom: 20, float: 'right' }}
                            size="large"
                            onClick={() => handleCreate()}
                        >
                            + New Movie
                        </Button>
                        <Table
                            columns={columns}
                            dataSource={dataMovie}
                            onChange={onChange}
                        />
                    </Content>
                </Layout>
            </Layout>
        </Layout>
        // <div className="container">
        //     <h1 style={{ textAlign: 'center' }}>Daftar Peserta Lomba</h1>
        //     <table>
        //         <thead>
        //             <tr>
        //                 <th>No</th>
        //                 <th>Title</th>
        //                 <th>Description</th>
        //                 <th>Year</th>
        //                 <th>Duration</th>
        //                 <th>Genre</th>
        //                 <th>Rating</th>
        //                 <th>Action</th>
        //             </tr>
        //         </thead>
        //         <tbody>
        //             {dataMovie !== null &&
        //                 dataMovie.map((item, index) => {
        //                     return (
        //                         <tr key={index}>
        //                             <td>{index + 1}</td>
        //                             <td>{item.title}</td>
        //                             <td title={item.description}>
        //                                 {truncateString(item.description, 20)}
        //                             </td>
        //                             <td>{item.year}</td>
        //                             <td>{item.duration}</td>
        //                             <td>{item.genre}</td>
        //                             <td>{item.rating}</td>
        //                             <td>
        //                                 <button
        //                                     onClick={handleEdit}
        //                                     value={item.id}
        //                                 >
        //                                     Edit
        //                                 </button>
        //                                 &nbsp;
        //                                 <button
        //                                     onClick={handleDelete}
        //                                     value={item.id}
        //                                 >
        //                                     Delete
        //                                 </button>
        //                             </td>
        //                         </tr>
        //                     );
        //                 })}
        //         </tbody>
        //     </table>
        // </div>
    );
};

export default MovieList;
