import React, { useContext, useState, useEffect } from 'react';
import { UserContext } from '../../../context/UserContext';
import axios from 'axios';
import { Link, useParams } from 'react-router-dom';
import { useHistory } from 'react-router-dom';

const GameForm = () => {
    let { id } = useParams();
    const history = useHistory();
    const [input, setInput] = useState({
        name: '',
        genre: '',
        singlePlayer: '',
        multiplayer: '',
        platform: '',
        release: 2020,
        image_url: '',
        id: null,
    });
    const [dataGameDetail, setDataGameDetail] = useState(null);
    const [user] = useContext(UserContext);

    useEffect(() => {
        if (dataGameDetail === null && id) {
            axios
                .get(`https://backendexample.sanbersy.com/api/data-game/${id}`)
                .then((res) => {
                    setDataGameDetail(res.data);
                    setInput({
                        name: res.data.name,
                        genre: res.data.genre,
                        singlePlayer: res.data.singlePlayer,
                        multiplayer: res.data.multiplayer,
                        platform: res.data.platform,
                        release: res.data.release,
                        image_url: res.data.image_url,
                        id: res.data.id,
                    });
                });
        }
    }, [dataGameDetail, setInput, id]);

    const handleChange = (event) => {
        let name = event.target.name;
        switch (name) {
            case 'name': {
                setInput({ ...input, name: event.target.value });
                break;
            }
            case 'genre': {
                setInput({ ...input, genre: event.target.value });
                break;
            }
            case 'singlePlayer': {
                setInput({ ...input, singlePlayer: event.target.value });
                break;
            }
            case 'multiplayer': {
                setInput({ ...input, multiplayer: event.target.value });
                break;
            }
            case 'platform': {
                setInput({ ...input, platform: event.target.value });
                break;
            }
            case 'release': {
                setInput({ ...input, release: event.target.value });
                break;
            }
            case 'image_url': {
                setInput({ ...input, image_url: event.target.value });
                break;
            }
            default: {
                break;
            }
        }
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        if (input.id === null) {
            axios
                .post(
                    `https://www.backendexample.sanbersy.com/api/data-game`,
                    {
                        name: input.name,
                        genre: input.genre,
                        singlePlayer: input.singlePlayer,
                        multiplayer: input.multiplayer,
                        platform: input.platform,
                        release: input.release,
                        image_url: input.image_url,
                    },
                    { headers: { Authorization: `Bearer ${user.token}` } }
                )
                .then((res) => {
                    console.log(res);
                    setDataGameDetail([
                        ...dataGameDetail,
                        { id: res.data.id, ...input },
                    ]);
                    setInput({
                        name: '',
                        genre: '',
                        singlePlayer: '',
                        multiplayer: '',
                        platform: '',
                        release: 2020,
                        image_url: '',
                    });
                    history.push(`/editdata/editgame`);
                })
                .catch((err) => {
                    console.log(err);
                });
        } else {
            axios
                .put(
                    `https://backendexample.sanbersy.com/api/data-game/${id}`,
                    {
                        name: input.name,
                        genre: input.genre,
                        singlePlayer: input.singlePlayer,
                        multiplayer: input.multiplayer,
                        platform: input.platform,
                        release: parseInt(input.release),
                        image_url: input.image_url,
                    },
                    { headers: { Authorization: `Bearer ${user.token}` } }
                )
                .then((res) => {
                    let newDataGames = [dataGameDetail].map((x) => {
                        if (x.id === { id }) {
                            x.name = input.name;
                            x.genre = input.genre;
                            x.singlePlayer = input.singlePlayer;
                            x.multiplayer = input.multiplayer;
                            x.platform = input.platform;
                            x.release = input.release;
                            x.image_url = input.image_url;
                        }
                        return x;
                    });
                    setDataGameDetail(newDataGames);
                    setInput({
                        name: '',
                        genre: '',
                        singlePlayer: '',
                        multiplayer: '',
                        platform: '',
                        release: 2020,
                        image_url: '',
                    });
                    history.push(`/editdata/editgame`);
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    };

    return (
        <div className="container">
            {user !== null && input !== null && (
                <>
                    {console.log(input.id)}
                    {console.log({ dataGameDetail })}
                    <h1 style={{ textAlign: 'center' }}>Games Form</h1>
                    <form
                        onSubmit={handleSubmit}
                        style={{
                            textAlign: 'center',
                            width: '60%',
                            margin: '0 auto',
                        }}
                    >
                        <div>
                            <label style={{ float: 'left' }}>Name:</label>
                            <input
                                style={{ float: 'right' }}
                                type="text"
                                name="name"
                                value={input.name}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <div style={{ marginTop: '20px' }}>
                            <label style={{ float: 'left' }}>Genre:</label>
                            <input
                                style={{ float: 'right' }}
                                type="text"
                                name="genre"
                                value={input.genre}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <div style={{ marginTop: '20px' }}>
                            <label style={{ float: 'left' }}>
                                Single Player:
                            </label>
                            <input
                                style={{ float: 'right' }}
                                type="boolean"
                                name="singlePlayer"
                                value={input.singlePlayer}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <div style={{ marginTop: '20px' }}>
                            <label style={{ float: 'left' }}>
                                Multiplayer:
                            </label>
                            <input
                                style={{ float: 'right' }}
                                type="number"
                                name="multiplayer"
                                value={input.multiplayer}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <div style={{ marginTop: '20px' }}>
                            <label style={{ float: 'left' }}>Platform:</label>
                            <input
                                style={{ float: 'right' }}
                                type="text"
                                name="platform"
                                value={input.platform}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <div style={{ marginTop: '20px' }}>
                            <label style={{ float: 'left' }}>Release:</label>
                            <input
                                style={{ float: 'right' }}
                                type="number"
                                max={2020}
                                min={1980}
                                name="release"
                                value={input.release}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <div style={{ marginTop: '20px' }}>
                            <label style={{ float: 'left' }}>Image Url:</label>
                            <textarea
                                style={{ float: 'right' }}
                                cols="50"
                                rows="3"
                                type="text"
                                name="image_url"
                                value={input.image_url}
                                onChange={handleChange}
                            />
                            <br />
                            <br />
                        </div>
                        <br />
                        <br />
                        <button>Submit</button>
                    </form>
                </>
            )}
        </div>
    );
};

export default GameForm;
