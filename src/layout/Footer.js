import React, { Component, useContext } from 'react';
import { Link } from 'react-router-dom';
import { UserContext } from '../context/UserContext';
import { Layout, Menu, Image } from 'antd';
import Logo from '../img/logo.png';
import '../App.css';

const { Footer } = Layout;

const logos = {
    margin: '0 24px 0 0',
    float: 'left',
};

class Foot extends Component {
    render() {
        return (
            <header>
                <Layout className="layout">
                    <Footer
                        style={{
                            textAlign: 'center',
                            backgroundColor: '#001529',
                            color: 'white',
                        }}
                    >
                        SanberCode ©2020 Bayu Aditya T
                    </Footer>
                </Layout>
            </header>
        );
    }
}

export default Foot;
