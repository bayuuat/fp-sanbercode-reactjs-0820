import React, { useContext } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Home from '../pages/home/Home';
import GameDetail from '../pages/detail/GameDetail';
import MovieDetail from '../pages/detail/MovieDetail';
import Login from '../pages/login/Login';
import Register from '../pages/register/Register';
import Dashboard from '../pages/editdata/ChangePass';
import MovieCreate from '../pages/editdata/editMovies/MovieForm';
import MovieEdit from '../pages/editdata/editMovies/MovieList';
import MovieEditForm from '../pages/editdata/editMovies/MovieForm';
import GameCreate from '../pages/editdata/editGames/GameForm';
import GameEdit from '../pages/editdata/editGames/GameList';
import GameEditForm from '../pages/editdata/editGames/GameForm';
import { UserContext } from '../context/UserContext';

const Section = () => {
    const [user] = useContext(UserContext);

    const PrivateRoute = ({ user, ...props }) => {
        if (user) {
            return <Route {...props} />;
        } else {
            return <Redirect to="/login" />;
        }
    };

    const LoginRoute = ({ user, ...props }) =>
        user ? <Redirect to="/" /> : <Route {...props} />;

    return (
        <section>
            <Switch>
                <Route exact path="/" user={user} component={Home} />
                <Route
                    exact
                    path="/movie/:id"
                    user={user}
                    component={MovieDetail}
                />
                <Route
                    exact
                    path="/game/:id"
                    user={user}
                    component={GameDetail}
                />
                <LoginRoute exact path="/login" user={user} component={Login} />
                <LoginRoute
                    exact
                    path="/register"
                    user={user}
                    component={Register}
                />
                <PrivateRoute
                    exact
                    path="/editdata"
                    user={user}
                    component={Dashboard}
                />
                <PrivateRoute
                    exact
                    path="/editdata/createmovie"
                    user={user}
                    component={MovieCreate}
                />
                <PrivateRoute
                    exact
                    path="/editdata/editmovie"
                    user={user}
                    component={MovieEdit}
                />
                <PrivateRoute
                    exact
                    path="/editdata/editmovie/:id"
                    user={user}
                    component={MovieEditForm}
                />
                <PrivateRoute
                    exact
                    path="/editdata/creategame"
                    user={user}
                    component={GameCreate}
                />
                <PrivateRoute
                    exact
                    path="/editdata/editgame"
                    user={user}
                    component={GameEdit}
                />
                <PrivateRoute
                    exact
                    path="/editdata/editgame/:id"
                    user={user}
                    component={GameEditForm}
                />
            </Switch>
        </section>
    );
};

export default Section;
